<?php
namespace App\Animal;
require_once 'Sound.php';
use App\Sound\Sound;
abstract class Animal{
    private $name;
    private $age;
    function __construct($name,$age){
        $this->name=$name;
        $this->age=$age;
    }
    abstract public  function makeSound(Sound $s);
}
?>