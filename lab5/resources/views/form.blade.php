<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lab5</title>
</head>
<body>
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="product" method="POST">
        {{ csrf_field() }}
        <input type="text" placeholder="Title" name="title">
        <input type="text" placeholder="Price" name="price">
        <input type="submit" value="OK">
    </form>
</body>
</html>