<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index(){
        return view('form');
    }
    public function store(Request $request){
       $request->validate([
            'title'=>'required|max:10',
            'price'=>'required|numeric|min:0|not in:0'
        ]);
        $title=$request->input('title');
        $price=$request->input('price');
        return view('product',[
            'title'=>$title,
            'price'=>$price
        ]);
    }
}
