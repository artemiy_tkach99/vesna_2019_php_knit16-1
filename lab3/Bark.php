<?php
namespace App\Bark;
require_once 'Sound.php';
use App\Sound\Sound;
class Bark implements Sound{
    public function spread(){
        echo "Woof!";
    }
} 
?>