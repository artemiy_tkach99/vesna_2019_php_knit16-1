<?php
namespace App\Dog;
require_once 'Animal.php';
require_once 'Sound.php';
use App\Sound\Sound;
use App\Animal\Animal;
class Dog extends Animal{
    public function makeSound(Sound $s){
        $s->spread();
    }
}
?>