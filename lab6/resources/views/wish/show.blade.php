@extends('layouts.app')

@section('content')
<table class="table table-striped">
<thead>
<th>Id</th>
<th>Title</th>
</thead>
<tbody>
    <tr>
    <td>{{$wish->id}}</td>
    <td>{{$wish->title}}</td>
    </tr>
</tbody>
</table>
@endsection